import requests
import json

schema = 'http://'
host = 'localhost'
port = 3000

class Endpoint:
  def __init__(self, path, method, params):
    self.path = path
    self.method = method
    self.params = params

endpoints = [
    Endpoint('/', 'GET', {}),
    Endpoint('/data', 'GET', {}),
    Endpoint('/name-query', 'GET', {
        'fname': 'Vasilii',
        'lname' : 'Chenosov'
    }),
    Endpoint('/rgb-to-hex', 'GET', {
        'red': 255,
        'green': 239,
        'blue': 0
    }),
    Endpoint('/add', 'POST', {
        'num1': 0.7,
        'num2': 30.6,
        'client_name': 'Kimi'
    }),
    Endpoint('/hex-to-rgb', 'POST', {
        'hex': '#ffef00'
    }),
]

for e in endpoints:
    url = schema + host + ':' + str(port) + e.path
    if e.method == 'GET':
        res = requests.get(url, params = e.params)
    if e.method == 'POST':
        res = requests.post(url, data=json.dumps(e.params), headers = {'content-type': 'application/json'})
    res.encoding = 'utf-8'
    print(res.text + '\n')