const express = require('express');
const bodyParser = require('body-parser')
const converter = require('./converter.js');
const app = express();
const port = 3000;
const jsonParser = bodyParser.json()

app.get('/', (req, res) => res.send('Got your message'));

app.get('/data', (req, res) => res.send('Here is your data: 123'));

app.get('/name-query', (req,res) => {
    const fname = req.query.fname;
    const lname =req.query.lname;
    res.send('Your name is '+ fname + lname);
})

app.get('/rgb-to-hex', (req,res) => {
    const red = req.query.red;
    const green =req.query.green;
    const blue =req.query.blue;
    hex = converter.rgbToHex(red, green, blue);
    res.send('HEX value: ' + hex);
})

app.post('/add', jsonParser, (req,res) => {
    if(!req.body) return res.sendStatus(400);

    var num1 = req.body.num1;
    var num2 = req.body.num2;
    var client_name = req.body.client_name;
    sum = num1 + num2;
    res.send('Client: '+ client_name + ' sum is ' + sum);
})

app.post('/hex-to-rgb', jsonParser, (req,res) => {
    if(!req.body) return res.sendStatus(400);

    const hex = req.body.hex;
    rgb = converter.hexToRgb(hex);
    res.send(JSON.stringify(rgb));
})

app.listen(port, () => console.log(`Listening on port ${port}`));