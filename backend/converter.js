module.exports = {
    rgbToHex: function (red, green, blue) {
        return "#" + componentToHex(red) + componentToHex(green) + componentToHex(blue);
    },
    hexToRgb: function (hex) {
        hex = s = hex.substring(1);
        var bigint = parseInt(hex, 16);
        var r = (bigint >> 16) & 255;
        var g = (bigint >> 8) & 255;
        var b = bigint & 255;
    
        return {
            "red" : r,
            "green" : g,
            "blue" : b
        };
    }
};

function componentToHex(c) {
    var hex = parseInt(c, 10);
    hex = hex.toString(16);
    return hex.length == 1 ? "0" + hex : hex;
  }
  